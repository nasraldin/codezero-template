﻿using CodeZero.Infrastructure.ClientMetaInfo;
using CodeZero.Infrastructure.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CodeZeroTemplate.API.Controllers
{
    public class ClientMetaInfoController : AnonymousController
    {
        [HttpGet("GeoInfo")]
        public async Task<ClientGeolocationInfo> GeoInfo()
        {
            // IPv4 or IPv6 Address
            // ex: 217.164.9.197 or 2001:8f8:1621:40ce:b814:7344:b758:6542
            return await LocationRequest.ClientGeoInfo("2001:8f8:1621:40ce:b814:7344:b758:6542");
        }

        [HttpGet("UserInfo")]
        public async Task<ClientInfo> UserInfo()
        {
            return await ClientInfoRequest.ClientInfo("217.164.9.197");
        }
    }
}