﻿using CodeZero.Infrastructure.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CodeZeroTemplate.API.Controllers
{
    [EnableCors("AllowAllFromLocalhost")] //CORS at specific Controller level
    public class CorsPolicyController : AnonymousController
    {
        [HttpGet]
        [Route("get")]
#pragma warning disable S3400 // Methods should not return constants
        public string Get()
#pragma warning restore S3400 // Methods should not return constants
        {
            return "Hello World";
        }

        [HttpGet]
        [Route("test2")]
        [EnableCors("Example3")]    //CORS at specific ROUTE level
#pragma warning disable S3400 // Methods should not return constants
        public string Test2()
#pragma warning restore S3400 // Methods should not return constants
        {
            return "Hello World";
        }

        [DisableCors]
        [HttpGet]
        [Route("GetValue")]
#pragma warning disable S3400 // Methods should not return constants
        public string GetValue()
#pragma warning restore S3400 // Methods should not return constants
        {
            return "value";
        }
    }
}