﻿using System.Threading.Tasks;
using CodeZeroTemplate.Entities;

namespace CodeZeroTemplate.Core.Security
{
    public interface IGetApiKeyQuery
    {
        Task<ApiKey?> Execute(string providedApiKey);
    }
}