﻿namespace CodeZeroTemplate.Core.Security
{
    public static class Roles
    {
        /// <summary>
        /// Supper Administrator Role
        /// </summary>
        public const string SupperAdmin = "SupperAdmin";

        /// <summary>
        /// Administrator Role
        /// </summary>
        public const string Admin = "Admin";

        /// <summary>
        /// Support Role
        /// </summary>
        public const string Support = "Support";

        /// <summary>
        /// Customer Role
        /// </summary>
        public const string Customer = "Customer";

        /// <summary>
        /// Company Role
        /// </summary>
        public const string Company = "Company";

        /// <summary>
        /// Individual Role
        /// </summary>
        public const string Individual = "Individual";

        /// <summary>
        /// Service Provider Role
        /// </summary>
        public const string ServiceProvider = "ServiceProvider";
    }
}