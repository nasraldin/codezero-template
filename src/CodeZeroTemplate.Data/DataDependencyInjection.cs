﻿using CodeZero.Configuration;
using CodeZeroTemplate.Data.Persistence.MariaDb;
using CodeZeroTemplate.Data.Persistence.Redis;
////using CodeZeroTemplate.Data.Persistence.SqlServer;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DataDependencyInjection
    {
        public static IServiceCollection AddData([NotNull] this IServiceCollection services, IConfiguration configuration)
        {
            if (CoreConfiguration.Instance.FeatureManagement.EnableDatabase)
                services.RegisterMariaDbContext(configuration);
            ////services.RegisterSqlServerDbContext(configuration); // Enable this line for sqlserver and disable RegisterMariaDbContext

            if (CoreConfiguration.Instance.FeatureManagement.EnableRedisCache)
                services.RegisterRedisCache(configuration);

            ////services.AddTransient<DbContextInitializer<ApplicationDbContext>>();

            return services;
        }
    }
}