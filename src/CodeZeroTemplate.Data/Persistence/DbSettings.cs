﻿namespace CodeZeroTemplate.Data.Persistence
{
    public class DbSettings
    {
        public string? ConnectionStrings { get; set; }
        public bool SensitiveDataLogging { get; set; } = false;
        public bool EnableDetailedErrors { get; set; } = false;
    }
}