﻿using System;
using CodeZero;
using CodeZero.Configuration;
using CodeZero.Data;
using CodeZeroTemplate.Data.AppDbContext;
using JetBrains.Annotations;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CodeZeroTemplate.Data.Persistence.SqlServer
{
    public static class RegisterSqlServer
    {
        public static IServiceCollection RegisterSqlServerDbContext([NotNull] this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStrings = configuration.GetSection(nameof(ConnectionStrings)).Get<ConnectionStrings>();
            var debugConfig = configuration.GetSection(nameof(DebugConfig)).Get<DebugConfig>();

            if (connectionStrings.SqlServer == null)
            {
                throw new CodeZeroException($"Verify SqlServer ConnectionStrings in appsetting.{Environment.GetEnvironmentVariable(AppConsts.ASPNETCORE_ENVIRONMENT)}.json, check appsettings.Template.json for example.");
            }

            var builder = new SqlConnectionStringBuilder(connectionStrings.SqlServer.DefaultConnection);

            var dbConfig = new DbSettings
            {
                ConnectionStrings = builder.ConnectionString,
                SensitiveDataLogging = debugConfig.SensitiveDataLogging,
                EnableDetailedErrors = debugConfig.EnableDetailedErrors
            };

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(dbConfig.ConnectionStrings,
                    sql =>
                    {
                        sql.MigrationsAssembly("CodeZeroTemplate.Data");
                        sql.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                    }
                );

                if (dbConfig.SensitiveDataLogging)
                {
                    options.EnableSensitiveDataLogging(dbConfig.SensitiveDataLogging);
                    options.UseLoggerFactory(LoggerFactory.Create(builder => { builder.AddConsole(); }));
                }

                if (dbConfig.EnableDetailedErrors)
                    options.EnableDetailedErrors(dbConfig.EnableDetailedErrors);
            });
            services.AddTransient<DbContextInitializer<ApplicationDbContext>>();

            return services;
        }
    }
}