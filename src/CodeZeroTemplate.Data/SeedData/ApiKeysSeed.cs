﻿using System.Collections.Generic;
using CodeZeroTemplate.Core.Security;
using CodeZeroTemplate.Entities;

namespace CodeZeroTemplate.Data.SeedData
{
    public static class ApiKeysSeed
    {
        public static List<ApiKey> MainApiKeys()
        {
            var listApiKeys = new List<ApiKey>
            {
                new ApiKey(
                    "Finance",
                    "6D181C28-4842-422D-A754-DB26075360D7",
                    new List<string>
                    {
                        Roles.Company,
                    }),
                new ApiKey(
                    "Reception",
                    "F05D2B7A-E657-4412-8CBB-0EA11324E211",
                    new List<string>
                    {
                        Roles.Individual,
                    }),
                new ApiKey(
                    "Management",
                    "B7C8079E-76F4-491B-B7C2-F39BEB1CDFAD",
                    new List<string>
                    {
                        Roles.Admin,
                        Roles.SupperAdmin
                    }),
                new ApiKey(
                    "Some Third Party",
                    "98FB6DE6-9459-4712-AFD6-8B4F321E6BA9",
                    new List<string>
                    {
                        Roles.ServiceProvider,
                    }),
            };

            return listApiKeys;
        }
    }
}