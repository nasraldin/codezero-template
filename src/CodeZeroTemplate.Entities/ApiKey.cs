﻿using System;
using System.Collections.Generic;
using CodeZero.Domain.Entities;
using CodeZero.Domain.Entities.Auditing;

namespace CodeZeroTemplate.Entities
{
    public class ApiKey : BaseEntity<int>, IActive, IAudited
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public ApiKey() { }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        public ApiKey(string owner, string key, IReadOnlyCollection<string> roles)
        {
            Owner = owner;
            Key = key;
            Roles = roles;
        }

        #region MainProps
        public string Owner { get; }
        public string Key { get; }
        public IReadOnlyCollection<string> Roles { get; }
        #endregion

        #region CheckIsActive
        public bool IsActive { get; set; }
        #endregion

        #region Auditing
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        #endregion

        protected sealed override bool Validate() => false;
    }
}