﻿using CodeZero;
using CodeZero.Configuration;
using CodeZero.ListStartupServices;
using Microsoft.Extensions.Hosting;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        // Register the Swagger generator and the Swagger UI middlewares
        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
        {
            Check.NotNull(app, nameof(app));

            if (AppSettings.Instance.Env.IsProduction() || AppSettings.Instance.Env.IsStaging())
            {
                // change to your exception handler!
                app.UseExceptionHandler("/Error");
            }

            app.UseCodeZeroHealthChecks();


            // Keep as it in last call
            if (AppSettings.Instance.Env.IsDevelopment() || AppSettings.Instance.Env.IsDev())
            {
                app.UseDeveloperExceptionPage();
                app.UseShowAllServicesMiddleware();
            }

            return app;
        }
    }
}