﻿using System.Linq;
using CodeZero.Configuration;
using CodeZero.Feature;
using CodeZeroTemplate.Data.AppDbContext;
using HealthChecks.UI.Client;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides an extension method for <see cref="CodeZeroBuilder"/>.
    /// </summary>
    public static partial class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add health checks.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/>.</param>
        public static IServiceCollection AddCodeZeroHealthChecks([NotNull] this IServiceCollection services)
        {
            var healthChecksIsEnabled = AppSettings.Instance.FeatureManager
                .IsEnabledAsync(nameof(FeatureFlags.CoreFeature.HealthChecks))
                .ConfigureAwait(false).GetAwaiter().GetResult();

            // Registers required services for health checks
            if (healthChecksIsEnabled)
            {
                var healthChecks = services.AddHealthChecks();

                if (CoreConfiguration.Instance.FeatureManagement.EnableDatabase)
                {
                    healthChecks.AddDbContextCheck<ApplicationDbContext>("ApplicationDbContext", tags: new[] { "DB" })
                        .AddMySql(CoreConfiguration.Instance.ConnectionStrings.MariaDB.DefaultConnection, "MariaDB Server", tags: new[] { "DB" });
                }

                if (CoreConfiguration.Instance.FeatureManagement.EnableRedisCache)
                    healthChecks.AddRedis(CoreConfiguration.Instance.RedisConfig.ConnectionString, "Redis Server", tags: new[] { "Cache" });

                if (CoreConfiguration.Instance.FeatureManagement.EnableSeq)
                    healthChecks.AddSeqPublisher(seq => { seq.Endpoint = CoreConfiguration.Instance.SeqOptions.Endpoint; }, "HealthChecks");

                var healthChecksUI = services.AddHealthChecksUI(opt =>
                {
                    opt.SetHeaderText(CoreConfiguration.Instance.HealthChecksUI.HeaderText);
                });

                if (CoreConfiguration.Instance.HealthChecksUI.EnableDatabaseStorage)
                {
                    healthChecksUI.AddMySqlStorage(CoreConfiguration.Instance.HealthChecksUI.StorageConnectionString);
                }
                else
                {
                    healthChecksUI.AddInMemoryStorage();
                }
            }

            return services;
        }
    }
}

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// Configure HTTP request pipeline for the current path.
    /// </summary>
    public static partial class ApplicationBuilderExtensions
    {
        // Register the Swagger generator and the Swagger UI middlewares
        public static IApplicationBuilder UseCodeZeroHealthChecks(this IApplicationBuilder app)
        {
            var healthChecksIsEnabled = AppSettings.Instance.FeatureManager
                .IsEnabledAsync(nameof(FeatureFlags.CoreFeature.HealthChecks))
                .ConfigureAwait(false).GetAwaiter().GetResult();

            if (healthChecksIsEnabled)
            {
                if (CoreConfiguration.Instance.HealthChecksUI.HealthChecks.Any())
                {
                    CoreConfiguration.Instance.HealthChecksUI.HealthChecks.ForEach(item =>
                    {
                        // HealthCheck middleware
                        app.UseHealthChecks(item.Uri, new HealthCheckOptions()
                        {
                            Predicate = _ => true,
                            ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                        });
                    });
                }

                // HealthCheck UI default is /healthchecks-ui/
                app.UseHealthChecksUI(opt =>
                {
                    opt.UIPath = CoreConfiguration.Instance.HealthChecksUI.UiEndpoint;
                    opt.AddCustomStylesheet($"wwwroot/healthchecks-ui/custom-healthchecks.css");
                });
            }

            return app;
        }
    }
}