﻿using CodeZero;
using Microsoft.AspNetCore.Authentication;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authentication
{
    public class ApiKeyAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DefaultScheme = AppConsts.AuthSchemes.ApiKey;

#pragma warning disable CA1822 // Mark members as static
        public string Scheme => DefaultScheme;
#pragma warning restore CA1822 // Mark members as static
#pragma warning disable S1104 // Fields should not have public accessibility
        public string AuthenticationType = DefaultScheme;
#pragma warning restore S1104 // Fields should not have public accessibility
    }
}