﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CodeZeroTemplate.Core.Security;
using CodeZeroTemplate.Data.SeedData;
using CodeZeroTemplate.Entities;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authentication
{
    public class InMemoryGetApiKeyQuery : IGetApiKeyQuery
    {
        private readonly List<ApiKey> _apiKeys;

        public InMemoryGetApiKeyQuery()
        {
            _apiKeys = ApiKeysSeed.MainApiKeys();
        }

        public Task<ApiKey?> Execute(string providedApiKey)
        {
            var key = _apiKeys.Find(k => k.Key == providedApiKey);
            return Task.FromResult(key);
        }
    }
}