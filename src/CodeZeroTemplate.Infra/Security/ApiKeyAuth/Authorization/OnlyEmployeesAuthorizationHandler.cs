﻿using System.Threading.Tasks;
using CodeZeroTemplate.Core.Security;
using Microsoft.AspNetCore.Authorization;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authorization
{
    public class OnlyEmployeesAuthorizationHandler : AuthorizationHandler<OnlyEmployeesRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OnlyEmployeesRequirement requirement)
        {
            if (context.User.IsInRole(Roles.Company))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}