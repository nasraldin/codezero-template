﻿using System.Threading.Tasks;
using CodeZeroTemplate.Core.Security;
using Microsoft.AspNetCore.Authorization;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authorization
{
    public class OnlyManagersAuthorizationHandler : AuthorizationHandler<OnlyManagersRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OnlyManagersRequirement requirement)
        {
            if (context.User.IsInRole(Roles.Admin))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}