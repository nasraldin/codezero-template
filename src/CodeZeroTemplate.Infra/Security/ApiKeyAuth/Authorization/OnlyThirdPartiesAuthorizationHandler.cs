﻿using System.Threading.Tasks;
using CodeZeroTemplate.Core.Security;
using Microsoft.AspNetCore.Authorization;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authorization
{
    public class OnlyThirdPartiesAuthorizationHandler : AuthorizationHandler<OnlyThirdPartiesRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OnlyThirdPartiesRequirement requirement)
        {
            if (context.User.IsInRole(Roles.ServiceProvider))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}