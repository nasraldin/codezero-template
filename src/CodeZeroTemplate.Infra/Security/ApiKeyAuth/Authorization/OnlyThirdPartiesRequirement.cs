﻿using Microsoft.AspNetCore.Authorization;

namespace CodeZeroTemplate.Infra.Security.ApiKeyAuth.Authorization
{
    public class OnlyThirdPartiesRequirement : IAuthorizationRequirement { }
}